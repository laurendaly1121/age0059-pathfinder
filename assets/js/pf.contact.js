/**
 * Created by jrodriguez on 9/12/16.
 */

var pf_contact = {};

pf_contact.isDeviceiPhone =  isMobile.apple.device;
pf_contact.pulseCount=0;
pf_contact.pulseMax=6;
pf_contact.pulseTimer = null;
pf_contact.pulseArray = [$('#F1'),$('#F2'),$('#F3'),$('#F4'),$('#F5'),$('#F6'),$('#F7'),$('#F8'),$('#F9'),$('#F10')];
pf_contact.lastNum=-1;
pf_contact.lastElement=$('#F5');

// INITIALIZE ON PAGE LOAD ------------------
$(document).ready(function() {

    // delay intro start by 1 second
    setTimeout(pf_contact.headerAnimation, 100);


    // bind the parallax plugin
    $('#header_container').parallaxDIV("50%", .90);




    // don't do this on iPhones
    if(!pf_contact.isDeviceiPhone) {

    }else{

    }


})




// helper functions --------------------------------




pf_contact.headerAnimation = function(){

    // trigger test animation ( velocity demos here: https://davidwalsh.name/svg-animation )
    $("#hand-l").delay(500)
        .velocity({strokeDashoffset: "0"}, {duration:2000})

    $("#hand-r").delay(1250)
        .velocity({strokeDashoffset: "0"}, {duration:2000})


    $("#header_title").delay(6000)
        .velocity({opacity: "1"}, {duration:2000});


    $("#hand-l").delay(3000)
        .velocity({opacity: "0"}, {duration:2000})

    $("#hand-r").delay(2000)
        .velocity({opacity: "0"}, {duration:2000})

    setTimeout(pf_contact.fingerAnimation, 3500);


    $("#light-left").delay(5000)
        .velocity({strokeDashoffset: "-2000", opacity:0}, {easing: "linear", duration:9000})


    $("#light-right").delay(5250)
        .velocity({strokeDashoffset: "-2000", opacity:0}, {easing: "linear", duration:9000})


}

pf_contact.fingerAnimation = function() {

    pf_contact.pulseTimer = setInterval(pf_contact.pulse, 500);

}

pf_contact.pulse = function(){

//    var element = pf_contact.pulseArray[pf_contact.pulseCount];

    // Pick a random number from 0 to 9
    var rn = Math.round(Math.random()*9);

    // No Repeats
    if(rn==pf_contact.lastNum){
        rn++;
        if(rn>9) rn=0;
    }

    // Remember last number so we can do No Repeat
    pf_contact.lastNum = rn;

    // Pull this finger
    var element = pf_contact.pulseArray[rn];

    var pos1 = pf_contact.lastElement.position();
    var pos2 = element.position();

    pf_contact.lastElement = element;

    /*
    var line = $('#connector');
   // line.attr('x1',pos1.left).attr('y1',pos1.top).attr('x2',pos2.left).attr('y2',pos2.top);
    line.velocity({opacity: ".5"}, {duration:1})
        .velocity({x1:pos1.left, x2:pos2.left, y1:pos1.top-75, y2:pos2.top-75, opacity:1}, {duration:300, ease:"easeInSine"})
        .velocity({opacity:0}, {duration:100, ease:"easeOutSine"})
*/

    // Animate the pulse
    element.velocity({opacity: ".7", scaleX:1.5, scaleY:1.5}, {duration:1})
        .velocity({opacity: "0", scaleX:.25, scaleY:.25}, {duration:800, ease:"easeOutSine"})

    // Count the pulses and stop after X
    if(pf_contact.pulseCount>=pf_contact.pulseMax){
        clearInterval(pf_contact.pulseTimer);
    }
    pf_contact.pulseCount++
}

