var pf_rental = {};

$(document).ready(function() {

    // delay intro start by 1 second
    setTimeout(pf_rental.headerAnimation, 1000);


    // bind the parallax plugin
    $('#header_container').parallaxDIV("50%", .90);

    // set up gallery
    $('.parent-container').magnificPopup({
      delegate: 'a', // child items selector, by clicking on it popup will open
      type: 'image',
      gallery:{
        enabled:true
      }
      // other options
    });

})


// helper functions --------------------------------

pf_rental.headerAnimation = function(){

    // trigger test animation ( velocity demos here: https://davidwalsh.name/svg-animation )
    $("#header_title").delay(1000)
        .velocity({opacity: "1"}, {duration:2000});

}


