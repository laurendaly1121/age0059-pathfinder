/**
 * Created by jrodriguez on 8/15/16.
 */

var pf_services = {};

pf_services.isDeviceiPhone =  isMobile.apple.device;

// INITIALIZE ON PAGE LOAD ------------------
$(document).ready(function() {

    // delay intro start by 1 second
    setTimeout(pf_services.headerAnimation, 100);


    // bind the parallax plugin
    $('#header_container').parallaxDIV("50%", .90);




    // don't do this on iPhones
    if(!pf_services.isDeviceiPhone) {

    }else{

    }


})




// helper functions --------------------------------




pf_services.headerAnimation = function(){

    // trigger test animation ( velocity demos here: https://davidwalsh.name/svg-animation )
  /*  $("#REDTAIL").delay(500)
        .velocity({strokeDashoffset: "0"}, {duration:1000})
*/

  var $StarsD = $("#STARS-0");
  var $StarsC = $("#STARS-1");
  var $StarsB = $("#STARS-2");
  var $StarsA = $("#STARS-3");


    $StarsA.velocity({  rotateZ: "-=360", opacity: "1" }, { duration: 10000, easing: "easeout"})
        .delay(-200)
        .velocity({   rotateZ: "+=45", opacity: "0" }, { duration: 2200, easing: "easein"})

    ;
    $StarsB.velocity({  rotateZ: "-=360", opacity: "1" }, { duration: 11000, easing: "easeout"} )
        .delay(-300)
        .velocity({  rotateZ: "+=45",  opacity: "0" }, { duration: 1800, easing: "easein"})
    ;
    $StarsC.velocity({  rotateZ: "-=360", opacity: "1" }, { duration: 12000, easing: "easeout"})
        .delay(-400)
        .velocity({  rotateZ: "+=45",  opacity: "0" }, { duration: 1400, easing: "easein"})
    ;
    $StarsD.velocity({  rotateZ: "-=360", opacity: "1" }, { duration: 13000, easing: "easeout"})
        .delay(-500)
        .velocity({   rotateZ: "+=45", opacity: "0" }, { duration: 1000, easing: "easein"})
    ;

    $("#header_title").delay(6000)
        .velocity({opacity: "1"}, {duration:2000});

    $("#REDTAIL").delay(6000)
        .velocity({opacity:1}, {easing: "easeout", duration:2500})
        .delay(5000)
        .velocity({opacity:0}, {easing: "easein", duration:2500})

    $("#light-left").delay(6000)
        .velocity({strokeDashoffset: "4000"}, {easing: "linear", duration:10000})


      $("#light-right").delay(4000)
        .velocity({strokeDashoffset: "4000"}, {easing: "linear", duration:10000})


    /*
    $("#light-right").delay(-1000)
        .velocity({opacity: "0"}, {easing: "none", duration:2000})
*/


}


