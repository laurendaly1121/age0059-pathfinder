/**
 * Created by jrodriguez on 8/15/16.
 */

var pf_ourteam = {};

pf_ourteam.isDeviceiPhone =  isMobile.apple.device;

var $bio1_l = $("#bio1_link"); /* ASHLEY */
var $bio2_l = $("#bio2_link"); /* BETH */
var $bio3_l = $("#bio3_link"); /* BRYAN */
var $bio4_l = $("#bio4_link"); /* ERIN */
var $bio5_l = $("#bio5_link"); /* JEFF */
var $bio6_l = $("#bio6_link"); /* LARRY */
var $bio7_l = $("#bio7_link"); /* RICH */
var $bio8_l = $("#bio8_link"); /* STEVE */

var $bio1_n = $("#bio1_name");
var $bio2_n = $("#bio2_name");
var $bio3_n = $("#bio3_name");
var $bio4_n = $("#bio4_name");
var $bio5_n = $("#bio5_name");
var $bio6_n = $("#bio6_name");
var $bio7_n = $("#bio7_name");
var $bio8_n = $("#bio8_name");

var $bio1_nf = $("#bio1_name_full").hide();
var $bio2_nf = $("#bio2_name_full").hide();
var $bio3_nf = $("#bio3_name_full").hide();
var $bio4_nf = $("#bio4_name_full").hide();
var $bio5_nf = $("#bio5_name_full").hide();
var $bio6_nf = $("#bio6_name_full").hide();
var $bio7_nf = $("#bio7_name_full").hide();
var $bio8_nf = $("#bio8_name_full").hide();


var $bio1_p = $("#bio1_photo");
var $bio2_p = $("#bio2_photo");
var $bio3_p = $("#bio3_photo");
var $bio4_p = $("#bio4_photo");
var $bio5_p = $("#bio5_photo");
var $bio6_p = $("#bio6_photo");
var $bio7_p = $("#bio7_photo");
var $bio8_p = $("#bio8_photo");

var $bio1_plus = $("#see-more-1-open");
var $bio2_plus = $("#see-more-2-open");
var $bio3_plus = $("#see-more-3-open");
var $bio4_plus = $("#see-more-4-open");
var $bio5_plus = $("#see-more-5-open");
var $bio6_plus = $("#see-more-6-open");
var $bio7_plus = $("#see-more-7-open");
var $bio8_plus = $("#see-more-8-open");

var bio_data = [
    {b_link:$bio1_l, b_name:$bio1_n, b_namefull:$bio1_nf, b_photo: $bio1_p, b_plus: $bio1_plus, b_isopen:false },
    {b_link:$bio2_l, b_name:$bio2_n, b_namefull:$bio2_nf, b_photo: $bio2_p, b_plus: $bio2_plus, b_isopen:false },
    {b_link:$bio3_l, b_name:$bio3_n, b_namefull:$bio3_nf, b_photo: $bio3_p, b_plus: $bio3_plus, b_isopen:false },
    {b_link:$bio4_l, b_name:$bio4_n, b_namefull:$bio4_nf, b_photo: $bio4_p, b_plus: $bio4_plus, b_isopen:false },
    {b_link:$bio5_l, b_name:$bio5_n, b_namefull:$bio5_nf, b_photo: $bio5_p, b_plus: $bio5_plus, b_isopen:false },
    {b_link:$bio6_l, b_name:$bio6_n, b_namefull:$bio6_nf, b_photo: $bio6_p, b_plus: $bio6_plus, b_isopen:false },
    {b_link:$bio7_l, b_name:$bio7_n, b_namefull:$bio7_nf, b_photo: $bio7_p, b_plus: $bio7_plus, b_isopen:false },
    {b_link:$bio8_l, b_name:$bio8_n, b_namefull:$bio8_nf, b_photo: $bio8_p, b_plus: $bio8_plus, b_isopen:false }
    ]

var group_data = [
    {id: 0, menutitle:"ALL", people:[0, 1, 2, 3, 4, 5, 6, 7]},
    {id: 1, menutitle:"BUSINESS INTELLIGENCE", people:[7, 1]},
    {id: 2, menutitle:"STRATEGY", people:[7, 2, 4]},
    {id: 3, menutitle:"RESEARCH", people:[6, 7, 3, 1, 2, 4]},
    {id: 4, menutitle:"OWNERSHIP", people:[0, 5]}
]

// INITIALIZE ON PAGE LOAD ------------------
$(document).ready(function() {

    // delay intro start by 1 second
    setTimeout(pf_ourteam.headerAnimation, 1000);


    // bind the parallax plugin
    $('#header_container').parallaxDIV("50%", .90);


    // set up custom accordion functionality - apply panel mouse events

    // don't do this on iPhones
    if(!pf_ourteam.isDeviceiPhone) {
        pf_ourteam.initPanelEvents();
    }else{
        // for iPhones, just show the long name
        for (id in bio_data) {
            pf_ourteam.animStateText(id, true);
            pf_ourteam.animStatePhoto(id, .75);
        }
    }

    // set up accordion sorting
    pf_ourteam.initSorting();

})




// helper functions --------------------------------



pf_ourteam.initSorting = function () {

    // assign mouse events to sub-nav

    $('.subnav-item').click(function () {

            // hide all them peeps
            $('.panel').hide();


            // selected state
            $('.subnav-item').removeClass('subnav-item-selected');
            $(this).addClass('subnav-item-selected');

            // pull the id data from the HTML ( data-groupid )
            var id = ($(this).data().groupid);

            // track as event in google
            ga('send', {
                hitType: 'event',
                eventCategory: 'OurTeam',
                eventAction: 'select_category',
                eventLabel: group_data[ id ].menutitle
            });

            var myPeeps = group_data[ id ].people;

            for (i in myPeeps) {
                var thisPeepsID = myPeeps[i];
                var thisDIV = "#biopanel"+thisPeepsID;
                var thisDIVreference = $(thisDIV);
                thisDIVreference.fadeIn(2000);
            }

        }
    )
}



pf_ourteam.initPanelEvents = function () {


    var id;

    for (id in bio_data) {

        var thisbio = bio_data[id];

        // rollover
        thisbio.b_link.mouseover(function() {$("#anim_contourback").delay(3000)
            .velocity({opacity: "0"}, {duration:2000});
            var id = ($(this).data().bioid);
            pf_ourteam.animStatePhoto(id, .5);
            pf_ourteam.animStateText(id, true);
        });

        // rollout
        thisbio.b_link.mouseout(function() {
            var id = ($(this).data().bioid);
            if(!bio_data[id].b_isopen) {
                pf_ourteam.animStatePhoto(id, 1);
                pf_ourteam.animStateText(id, false);
            }
        });

        thisbio.b_link.click(function() {

            var id = ($(this).data().bioid);
            var trackname = ($(this).data().biotrack);

            // toggle the state when clicked
            if(bio_data[id].b_isopen){
                pf_ourteam.clearAllBios(id);
                bio_data[id].b_isopen = false;
                pf_ourteam.animStatePhoto(id, .5);
                bio_data[id].b_plus.attr("src","assets/images/plus.png");
            }else{
                pf_ourteam.clearAllBios(id);
                bio_data[id].b_isopen = true;
                pf_ourteam.animStatePhoto(id, .5);

                bio_data[id].b_plus.attr("src","assets/images/minus.png");

                // track as event in google
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'OurTeam',
                    eventAction: 'open_bio',
                    eventLabel: trackname
                });
            }

        });

    }

}
// animate the state of the photo
pf_ourteam.animStatePhoto = function (id, op) {
    bio_data[id].b_photo.stop(true, false).animate({opacity:op}, 400, "easeOutQuad");
}
// animate the state of the text
pf_ourteam.animStateText = function (id, isShowFull) {
    if(isShowFull){
        bio_data[id].b_namefull.show();
        bio_data[id].b_name.hide();
    }else{
        bio_data[id].b_name.show();
        bio_data[id].b_namefull.hide();
    }

}

// reset all of the states (used when clicking)
pf_ourteam.clearAllBios = function (trigger_id) {

    // reset all bios states
    var id;
    for (id in bio_data) {
        pf_ourteam.animStatePhoto(id, 1);
        bio_data[id].b_isopen = false;
        if(id!=trigger_id){
            pf_ourteam.animStateText(id, false);
        }
    }
}

pf_ourteam.headerAnimation = function(){

    // trigger test animation ( velocity demos here: https://davidwalsh.name/svg-animation )
    $("#ridgeline1").delay(500)
        .velocity({strokeDashoffset: "0"}, {duration:1000})

    $("#ridgeline2").delay(1000)
        .velocity({strokeDashoffset: "0"}, {duration:600})


    $(".contourbackline").each(function( index ) {
        $(this).delay(2300+((index*50)))
                .velocity({opacity: "1"}, {duration:350})
               //.velocity({strokeDashoffset: "0"}, {duration:1000})
    });

    $(".contourfrontleft").each(function( index ) {
        $(this).delay(2000+((index*50)))
                .velocity({opacity: "1"}, {duration:350})
              // .velocity({strokeDashoffset: "0"}, {duration:500})
    });

    $(".contourfrontright").each(function( index ) {
        $(this).delay(2000+((index*50)))
                .velocity({opacity: "1"}, {duration:350})
              // .velocity({strokeDashoffset: "0"}, {duration:500})
    });

    $("#ridge").delay(3200)
        .velocity({opacity: "0"}, {duration:2000});
    $("#anim_contourfront_left").delay(3000)
        .velocity({opacity: "0"}, {duration:2000});
    $("#anim_contourfront_right").delay(3000)
        .velocity({opacity: "0"}, {duration:2000});
    $("#anim_contourback").delay(3000)
        .velocity({opacity: "0"}, {duration:2000});

    $("#header_title").delay(4000)
        .velocity({opacity: "1"}, {duration:2000});


    $("#pathlines").delay(4000)
        .velocity({strokeDashoffset: "0"}, {easing: "none", duration:2500})
        .velocity({strokeDashoffset: "-500"}, {duration:1000});

    $("#pathlines").delay(-1000)
        .velocity({opacity: "0"}, {easing: "none", duration:2000})


    $(".pathdots").each(function( index ) {

        var innercircles = $(this)[0].childNodes;

        $(innercircles[1]).delay(6000-((index*400)))
            .velocity({r: "15"}, {duration:800})
        $(innercircles[2]).delay(6000-((index*400)))
            .velocity({r: "15"}, {duration:800})

        $(this).delay(6000-((index*400)))
            .velocity({opacity: "1"}, {duration:100})
            .delay(500)
            .velocity({opacity: "0"}, {duration:600})

    });


}


