var pf_homepage = {};

$(document).ready(function() {

    // delay intro start by 1 second
    setTimeout(pf_homepage.headerAnimation, 1000);


    // bind the parallax plugin
    $('#header_container').parallaxDIV("50%", .90);

    // set up custom hover functionality
    $("#bulb_link").mouseenter(function () {
        $('img', this).attr("src", "assets/images/bulb-animation.gif");
    }).mouseleave(function () {
        $('img', this).attr("src", "assets/images/bulb.png");
    });
    $("#line_link").mouseenter(function () {
        $('img', this).attr("src", "assets/images/linegraph-animation.gif");
    }).mouseleave(function () {
        $('img', this).attr("src", "assets/images/linegraph.png");
    });
    $("#pres_link").mouseenter(function () {
        $('img', this).attr("src", "assets/images/presentation-animation.gif");
    }).mouseleave(function () {
        $('img', this).attr("src", "assets/images/presentation.png");
    });

})


// helper functions --------------------------------

pf_homepage.headerAnimation = function(){

    // trigger test animation ( velocity demos here: https://davidwalsh.name/svg-animation )
    $("#leftridge").delay(500)
        .velocity({strokeDashoffset: "0"}, {duration:1000})

    $("#rightridge").delay(1250)
        .velocity({strokeDashoffset: "0"}, {duration:600})

    $(".contourleft_line").each(function( index ) {
        $(this).delay(2000+((index*50)))
                .velocity({opacity: "1"}, {duration:350})

    });
    $(".contourright_line").each(function( index ) {
        $(this).delay(2000+((index*50)))
                .velocity({opacity: "1"}, {duration:350})

    });


    $("#ridge").delay(3200)
        .velocity({opacity: "0"}, {duration:2000});
    $("#contourleft").delay(3000)
        .velocity({opacity: "0"}, {duration:2000});
    $("#contourright").delay(3000)
        .velocity({opacity: "0"}, {duration:2000});

    $("#header_title").delay(4000)
        .velocity({opacity: "1"}, {duration:2000});


    // then logo here
    $(".logolline_white").delay(3500)
        .velocity({strokeDashoffset: "0"}, {duration:1650})
        .velocity({fillOpacity:"1"}, {duration:500, ease:"easeOutSine"})

    $(".logolline_red").delay(5150)
        .velocity({fillOpacity:"1"}, {duration:450, ease:"easeOutSine"})

    $("#svglogo_subtext").delay(5100)
        .velocity({opacity: "1"}, {duration:500});


}


